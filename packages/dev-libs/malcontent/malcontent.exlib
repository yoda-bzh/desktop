# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix='https://gitlab.freedesktop.org' user='pwithnall' new_download_scheme=true ]
require meson [ meson_minimum_version=1.2.0 ]
require test-dbus-daemon
require gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="malcontent implements parental controls support"
DESCRIPTION="
It can be used by applications to filter or limit the access of child accounts
to inappropriate content."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gui [[
        description = [ Build widget library and GUI application for parental control settings ]
    ]]
"

DEPENDENCIES="
    build:
        dev-util/itstool
        gnome-desktop/gobject-introspection:1
        virtual/pkg-config
        sys-devel/gettext
    build+test:
        dev-libs/libxml2:2.0
    build+run:
        dev-libs/glib:2[>=2.60.0]
        dev-libs/libglib-testing
        sys-apps/dbus
        sys-auth/polkit:1
        sys-libs/pam
        gui? (
            dev-libs/appstream:=[>=0.12.10]
            dev-libs/libadwaita:1[>=1.6][gobject-introspection]
            sys-apps/accountsservice[>=0.6.39][gobject-introspection]
            sys-apps/flatpak
            x11-libs/gtk:4.0[>=4.12]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dpamlibdir=/usr/$(exhost --target)/lib

    -Dinstalled_tests=false
    -Duse_system_libmalcontent=false
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gui ui'
)

malcontent_pkg_postinst() {
    option gui && gtk-icon-cache_pkg_postinst
}

malcontent_pkg_postrm() {
    option gui && gtk-icon-cache_pkg_postrm
}

