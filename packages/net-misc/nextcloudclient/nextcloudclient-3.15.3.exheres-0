# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2

require github [ user=nextcloud project=desktop tag=v${PV} ]
require cmake [ ninja=true ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

SUMMARY="Nextcloud themed desktop client"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"

MYOPTIONS="
    dolphin [[ description = [ Build dolphin extensions ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

QT_MIN_VER="6.7.0"
KF5_MIN_VER="5.240.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:6
        || (
            media-gfx/inkscape
            gnome-desktop/librsvg:2
        ) [[ note = [ inkscape (checked first) or rsvg-convert to generate icons ] ]]
    build+run:
        dev-db/sqlite:3[>=3.9.0]
        kde-frameworks/karchive:6
        kde-frameworks/kguiaddons:6
        sys-auth/qtkeychain[providers:qt6]
        sys-libs/zlib
        x11-libs/qt5compat:6[>=${QT_MIN_VER}]
        x11-libs/qtbase:6[>=${QT_MIN_VER}][gui][sql][sqlite]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
        x11-libs/qtwebsockets:6[>=${QT_MIN_VER}]
        dolphin? (
            kde-frameworks/kcoreaddons:6[>=${KF5_MIN_VER}]
            kde-frameworks/kio:6[>=${KF5_MIN_VER}]
            x11-libs/qtbase:6[>=6.6.0]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1] )
    test:
        dev-util/cmocka
    suggestion:
        dev-python/nautilus-python [[ description = [ To enable sync icons in
            nautilus indicating which files have been synched and which haven't been ] ]]
"

CMAKE_SOURCE="${WORKBASE}"/desktop-${PV}

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'dolphin BUILD_SHELL_INTEGRATION_DOLPHIN'
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_UPDATER:BOOL=FALSE
    -DBUILD_WITH_WEBENGINE:BOOL=FALSE
    -DCMAKE_DISABLE_FIND_PACKAGE_KF5KIO:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DENABLE_CLANG_TIDY:BOOL=FALSE
    -DENFORCE_SINGLE_ACCOUNT:BOOL=FALSE
    -DNEXTCLOUD_DEV:BOOL=FALSE
    -DSHARE_INSTALL_PREFIX:PATH=/usr/share
    -DSYSCONF_INSTALL_DIR:PATH=/etc
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_test() {
    export HOME="${TEMP}"

    # https://doc.qt.io/qt-6/qpa.html#qpa-minimal-plugins
    export QT_QPA_PLATFORM=minimal

    esandbox allow_net "unix:${TEMP}/runtime-paludisbuild/Nextcloud/socket"
    esandbox allow_net "LOOPBACK@12345"
    edo ninja test
    esandbox disallow_net "LOOPBACK@12345"
    esandbox disallow_net "unix:${TEMP}/runtime-paludisbuild/Nextcloud/socket"
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

