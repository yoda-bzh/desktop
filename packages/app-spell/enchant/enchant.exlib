# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=rrthomas release=v${PV} suffix=tar.gz ]

SUMMARY="Wrapper for spell checking libraries"

LICENCES="LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        app-spell/aspell
        app-spell/hunspell:=
        dev-libs/glib:2[>=2.6]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

if ever at_least 2.6.7; then
    # Require UnitTest++[>=1.6]
    RESTRICT="test"

    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --with-aspell
        --with-hunspell
        --without-applespell
        --without-hspell
        --without-nuspell
        --without-voikko
        --without-zemberek
    )
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --prefix=/usr
        --exec-prefix=/usr/$(exhost --target)
        --includedir=/usr/$(exhost --target)/include
        --with-aspell-prefix=/usr/$(exhost --target)
        --with-system-myspell=yes
        --enable-aspell
        --enable-myspell
        --disable-hspell
        --disable-ispell
        --disable-uspell
        --disable-voikko
        --disable-zemberek
    )
fi

