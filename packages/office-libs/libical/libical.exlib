# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013, 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require vala [ vala_dep=true with_opt=true option_name=vapi ]
require github [ tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_compile src_install

SUMMARY="An implementation of the IETF's iCalendar Calendaring and Scheduling protocols"
DESCRIPTION="
ibical is an Open Source implementation of the IETF's iCalendar Calendaring and Scheduling protocols.
(RFC 2445, 2446, and 2447).
It parses iCal components and provides C/C++/Python/Java APIs for manipulating the component properties,
parameters, and subcomponents.
"

LICENCES="|| ( MPL-1.1 LGPL-2.1 )"
SLOT="$(ever major)"
MYOPTIONS="
    doc
    examples
    gobject-introspection

    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-libs/icu:=   [[ note = [ higly recommended RSCALE support ] ]]
        dev-libs/libxml2:2.0[>=2.7.3]
        gobject-introspection? (
            gnome-desktop/gobject-introspection:1[>=0.6.7]
        )
    run:
        sys-libs/timezone-data
        !office-libs/libical:0[<2.0.0-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_BerkeleyDB:BOOL=true
    # Fails to build: https://github.com/libical/libical/issues/390
    -DENABLE_GTK_DOC:BOOL=false
    -DICAL_ERRORS_ARE_FATAL:BOOL=false
    -DICAL_GLIB:BOOL=true
    -DSHARE_INSTALL_DIR=/usr/share
    -DSHARED_ONLY:BOOL=TRUE
    -DUSE_BUILTIN_TZDATA:BOOL=false
    -DWITH_CXX_BINDINGS:BOOL=true
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples LIBICAL_BUILD_EXAMPLES'
    'gobject-introspection GOBJECT_INTROSPECTION'
    'vapi ICAL_GLIB_VAPI'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( 'doc Doxygen' )

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLIBICAL_BUILD_TESTING=true -DLIBICAL_BUILD_TESTING=false'
)

# One test fails when running the test suite in parallel
CMAKE_SRC_TEST_PARAMS=( -j1 )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    doc/AddingOrModifyingComponents.txt
    doc/UsingLibical.md
)

libical_src_compile() {
    cmake_src_compile
    option doc && emake docs
}

libical_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/cmake/LibIcal      LibIcal-${SLOT}
        /usr/${host}/lib/${PN}_cxx.so       ${PN}_cxx-${SLOT}.so
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/${PN}ss.so         ${PN}ss-${SLOT}.so
        /usr/${host}/lib/${PN}ss_cxx.so     ${PN}ss_cxx-${SLOT}.so
        /usr/${host}/lib/${PN}vcal.so       ${PN}vcal-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if option doc ; then
        dodoc -r apidocs/html
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

