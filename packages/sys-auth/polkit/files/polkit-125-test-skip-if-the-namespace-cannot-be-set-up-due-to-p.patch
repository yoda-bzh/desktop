Upstream: yes
Reason: Fixes the tests with python:3.12 amongst other things

From f0b67353240ec8f8d24c2666d74560d93996c1c5 Mon Sep 17 00:00:00 2001
From: Luca Boccassi <bluca@debian.org>
Date: Thu, 8 Aug 2024 17:51:39 +0100
Subject: [PATCH 1/2] test: skip if the namespace cannot be set up due to
 permissions

On some build systems, like Debian's, the tests do not have permissions to
create new namespaces, so skip gracefully in that case

Follow-up for 8e17f09c770bc2efd5deb40ba2b6032d40603578
---
 test/wrapper.py | 26 +++++++++++++++-----------
 1 file changed, 15 insertions(+), 11 deletions(-)

diff --git a/test/wrapper.py b/test/wrapper.py
index daaa40a..4f24c1f 100755
--- a/test/wrapper.py
+++ b/test/wrapper.py
@@ -17,17 +17,21 @@ def setup_test_namespace(data_dir):
     # Setup a new mount & user namespace, so we can use mount() unprivileged (see user_namespaces(7))
     euid = os.geteuid()
     egid = os.getegid()
-    os.unshare(os.CLONE_NEWNS|os.CLONE_NEWUSER)
-    # Map root to the original EUID and EGID, so we can actually call mount() inside our namespace
-    with open("/proc/self/uid_map", "w") as f:
-        f.write(f"0 {euid} 1")
-    with open("/proc/self/setgroups", "w") as f:
-        f.write("deny")
-    with open("/proc/self/gid_map", "w") as f:
-        f.write(f"0 {egid} 1")
-
-    # Overmount /etc with our own version
-    subprocess.check_call(["mount", "--bind", os.path.join(data_dir, "etc"), "/etc"])
+    try:
+        os.unshare(os.CLONE_NEWNS|os.CLONE_NEWUSER)
+        # Map root to the original EUID and EGID, so we can actually call mount() inside our namespace
+        with open("/proc/self/uid_map", "w") as f:
+            f.write(f"0 {euid} 1")
+        with open("/proc/self/setgroups", "w") as f:
+            f.write("deny")
+        with open("/proc/self/gid_map", "w") as f:
+            f.write(f"0 {egid} 1")
+
+        # Overmount /etc with our own version
+        subprocess.check_call(["mount", "--bind", os.path.join(data_dir, "etc"), "/etc"])
+    except PermissionError:
+        print("Lacking permissions to set up test harness, skipping")
+        sys.exit(77)
 
 if __name__ == "__main__":
     parser = argparse.ArgumentParser()
-- 
2.47.0


From a45a2a1ec0847069aa71269472692584af93bca3 Mon Sep 17 00:00:00 2001
From: Luca Boccassi <bluca@debian.org>
Date: Thu, 8 Aug 2024 19:35:36 +0100
Subject: [PATCH 2/2] test: skip if os.unshare is not available

It was added in Python 3.12 which is very recent, so skip
gracefully if it is not available, like on the current
Debian stable or Ubuntu Jammy

https://docs.python.org/3/library/os.html#os.unshare
---
 test/wrapper.py | 3 +++
 1 file changed, 3 insertions(+)

diff --git a/test/wrapper.py b/test/wrapper.py
index 4f24c1f..14f4abd 100755
--- a/test/wrapper.py
+++ b/test/wrapper.py
@@ -32,6 +32,9 @@ def setup_test_namespace(data_dir):
     except PermissionError:
         print("Lacking permissions to set up test harness, skipping")
         sys.exit(77)
+    except AttributeError:
+        print("Python 3.12 is required for os.unshare(), skipping")
+        sys.exit(77)
 
 if __name__ == "__main__":
     parser = argparse.ArgumentParser()
-- 
2.47.0

