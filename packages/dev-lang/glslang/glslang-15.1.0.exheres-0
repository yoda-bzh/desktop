# Copyright 2018-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup tag=${PV} ] cmake

SUMMARY="Khronos reference front-end for GLSL and ESSL, and sample SPIR-V generator"
DESCRIPTION="
An OpenGL and OpenGL ES shader front end and validator. There are several components:
* A GLSL/ESSL front-end for reference validation and translation of GLSL/ESSL into an AST.
* An HLSL front-end for translation of a broad generic HLL into the AST. See issue 362 and issue
  701 for current status.
* A SPIR-V back end for translating the AST to SPIR-V.
* A standalone wrapper, glslangValidator, that can be used as a command-line tool for the above.
"
HOMEPAGE+=" https://www.khronos.org/opengles/sdk/tools/Reference-Compiler"

LICENCES="Apache-2.0 BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-lang/spirv-tools[>=2024.4-rc2]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DALLOW_EXTERNAL_SPIRV_TOOLS:BOOL=TRUE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DBUILD_EXTERNAL:BOOL=TRUE
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DBUILD_WERROR:BOOL=FALSE
    -DENABLE_EMSCRIPTEN_ENVIRONMENT_NODE:BOOL=FALSE
    -DENABLE_EMSCRIPTEN_SINGLE_FILE:BOOL=FALSE
    -DENABLE_EXCEPTIONS:BOOL=FALSE
    -DENABLE_GLSLANG_BINARIES:BOOL=TRUE
    -DENABLE_GLSLANG_JS:BOOL=FALSE
    -DENABLE_HLSL:BOOL=TRUE
    -DENABLE_OPT:BOOL=TRUE
    -DENABLE_PCH:BOOL=TRUE
    -DENABLE_RTTI:BOOL=FALSE
    -DENABLE_SPIRV:BOOL=TRUE
    -DENABLE_SPVREMAPPER:BOOL=TRUE
    -DGLSLANG_ENABLE_INSTALL:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DGLSLANG_TESTS:BOOL=TRUE -DGLSLANG_TESTS:BOOL=FALSE'
)

