# Copyright 2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=heftig release=v${PV} suffix=tar.xz ] \
    meson \
    systemd-service

SUMMARY="Realtime Policy and Watchdog Daemon"

LICENCES="BSD-0 GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    systemd
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        group/rtkit
        user/rtkit
        sys-apps/dbus
        sys-auth/polkit:1
        sys-libs/libcap
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.13-pr17.patch
    "${FILES}"/${PN}-0.13-pr18.patch
    "${FILES}"/${PN}-0.13-pr19.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddbus_interfacedir=/usr/share/dbus-1/interfaces
    -Ddbus_rulesdir=/usr/share/dbus-1/system.d
    -Ddbus_systemservicedir=/usr/share/dbus-1/system-services
    -Dinstalled_tests=false
    -Dpolkit_actiondir=/usr/share/polkit-1/actions
    -Dsystemd_systemunitdir=${SYSTEMDSYSTEMUNITDIR}
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'systemd libsystemd'
)

