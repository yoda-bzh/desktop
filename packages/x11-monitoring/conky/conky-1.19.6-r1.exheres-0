# Copyright 2008 Richard Brown
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'conky-1.5.2_pre01116.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require cmake
require lua [ multibuild=false with_opt=false whitelist='5.3 5.4' ]
require github [ user=brndnmtthws tag=v${PV} ]

SUMMARY="A free, light-weight system monitor"
DESCRIPTION="Conky has more than 250 built in objects, including support for:
* a plethora of OS stats (uname, uptime, CPU usage, mem usage, disk usage, 'top' like process stats, and network monitoring, just to name a few)
* built in IMAP and POP3 support
* built in support for many popular music players (MPD, XMMS2, BMPx, Audacious)

Conky can display this info either as text, or using simple progress bars and graph widgets, with different fonts and colours.
"

LICENCES="GPL-3 BSD-3 LGPL-2.1 MIT vim-syntax? ( vim )"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation.html"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="truetype vim-syntax X
    audacious [[ description = [ Enables support for the audacious audio player ] ]]
    hddtemp [[ description = [ Enables support for the hddtemp daemon ] ]]
    ibm [[ description = [ Enables support for ibm/lenovo notebooks ] ]]
    imlib2 [[ description = [ Enable imlib2 support to display images ]
              requires = X
           ]]
    lua-cairo [[ description = [ Cairo bindings for lua ] requires = [ X ] ]]
    lua-imlib2 [[ description = [ Imlib2 Bindings for lua ] requires = [ imlib2 ] ]]
    rss [[ description = [ Enables downloading and parsing of rss feeds ] ]]
    truetype [[ requires = X ]]
    wifi [[ description = [ Enables support for wireless tools ] ]]
"

# bundles toluapp
DEPENDENCIES="
    build:
        virtual/pkg-config
        X? (
            x11-libs/libXt
            x11-proto/xorgproto
        )
    build+run:
        sys-libs/ncurses
        audacious? ( media-sound/audacious )
        hddtemp? ( hardware-monitoring/hddtemp )
        rss? (
            dev-libs/libxml2:2.0
            net-misc/curl
        )
        wifi? ( net-wireless/wireless_tools )
        X? (
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libXdamage
            x11-libs/libXext
            x11-libs/libXfixes
            x11-libs/libXft
            x11-libs/libXinerama
            x11-libs/libXrender
            imlib2? ( media-libs/imlib2[>=1.4.2][X] )
            lua-cairo? ( x11-libs/cairo[X] )
            truetype? (
                media-libs/freetype:2
                media-libs/fontconfig
            )
        )
"

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream
    edo sed \
        -e 's:DESTINATION share:DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i data/CMakeLists.txt
}

src_configure() {
    local cmakeparams=(
        -DDOC_PATH:PATH=/usr/share/doc/${PNVR}
        -DLOCALE_DIR:PATH=/usr/share/locale
        -DLUA_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/lua${LUA_ABIS}
        -DLUA_LIBRARY:PATH=/usr/$(exhost --target)/lib/liblua${LUA_ABIS}.so
        -DCHECK_CODE_QUALITY:BOOL=FALSE
        -DBUILD_APCUPSD:BOOL=FALSE
        -DBUILD_BUILTIN_CONFIG:BOOL=TRUE
        -DBUILD_CMUS:BOOL=FALSE
        -DBUILD_DOCS:BOOL=FALSE
        -DBUILD_EXTRAS:BOOL=FALSE
        -DBUILD_HTTP:BOOL=FALSE
        -DBUILD_I18N:BOOL=TRUE
        -DBUILD_ICAL:BOOL=FALSE
        -DBUILD_ICONV:BOOL=FALSE
        -DBUILD_INTEL_BACKLIGHT:BOOL=FALSE
        -DBUILD_IOSTATS:BOOL=TRUE
        -DBUILD_IPV6:BOOL=TRUE
        -DBUILD_IRC:BOOL=FALSE
        -DBUILD_JOURNAL:BOOL=FALSE
        -DBUILD_LUA_RSVG:BOOL=FALSE
        -DBUILD_MATH:BOOL=FALSE
        -DBUILD_MOC:BOOL=FALSE
        -DBUILD_MPD:BOOL=FALSE
        -DBUILD_MYSQL:BOOL=FALSE
        -DBUILD_NCURSES:BOOL=TRUE
        -DBUILD_OLD_CONFIG:BOOL=TRUE
        -DBUILD_PORT_MONITORS:BOOL=FALSE
        -DBUILD_PULSEAUDIO:BOOL=FALSE
        -DBUILD_WAYLAND:BOOL=FALSE
        -DBUILD_XMMS2:BOOL=FALSE
        $(cmake_build audacious AUDACIOUS)
        $(cmake_build hddtemp HDDTEMP)
        $(cmake_build ibm IBM)
        $(cmake_build imlib2 IMLIB2)
        $(cmake_build lua-cairo LUA_CAIRO)
        $(cmake_build lua-imlib2 LUA_IMLIB2)
        $(cmake_build rss RSS)
        $(cmake_build rss CURL)
        $(cmake_build wifi WLAN)
    )

    if option X; then
        cmakeparams+=(
            -DBUILD_ARGB:BOOL=TRUE
            -DBUILD_X11:BOOL=TRUE
            -DBUILD_XDAMAGE:BOOL=TRUE
            -DBUILD_XDBE:BOOL=TRUE
            -DBUILD_XFIXES:BOOL=TRUE
            -DBUILD_XFT:BOOL=TRUE
            -DBUILD_XINERAMA:BOOL=TRUE
            -DBUILD_XSHAPE:BOOL=TRUE
            -DBUILD_MOUSE_EVENTS:BOOL=TRUE
            -DOWN_WINDOW:BOOL=TRUE
        )
    else
        cmakeparams+=( -DBUILD_X11:BOOL=FALSE )
    fi

    ecmake "${cmakeparams[@]}"
}

src_install() {
    cmake_src_install

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/ftdetect
        doins "${WORK}"/extras/vim/ftdetect/conkyrc.vim

        insinto /usr/share/vim/vimfiles/syntax
        edo mv "${WORK}"/extras/vim/syntax/conkyrc.vim{.j2,}
        doins "${WORK}"/extras/vim/syntax/conkyrc.vim
    fi
}

