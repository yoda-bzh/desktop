# Copyright 2018-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=Vulkan-Headers tag=v${PV} ] \
    cmake

SUMMARY="Vulkan Header files and API registry"
DESCRIPTION="
Contains the Vulkan header files and the Vulkan API definition (registry) with its related files.
"
HOMEPAGE+=" https://www.khronos.org/vulkan"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        !sys-libs/vulkan [[
            description = [ The vulkan package has been split into individual packages ]
            resolution = uninstall-blocked-after
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DVULKAN_HEADERS_ENABLE_INSTALL:BOOL=TRUE
    -DVULKAN_HEADERS_ENABLE_MODULE:BOOL=FALSE
    -DVULKAN_HEADERS_ENABLE_TESTS:BOOL=FALSE
)

src_prepare() {
    cmake_src_prepare

    # install cmake files into arch-dependent location
    edo sed \
        -e 's:share/cmake:${CMAKE_INSTALL_LIBDIR}/cmake:g' \
        -i CMakeLists.txt
}

