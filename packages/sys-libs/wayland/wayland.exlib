# Copyright 2012 Arne Janbu
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" suffix=tar.bz2 new_download_scheme=true $(ever is_scm && echo branch=main) ]
require meson

export_exlib_phases src_prepare src_test

SUMMARY="The Wayland display protocol library (client and server)"
HOMEPAGE+=" https://wayland.freedesktop.org/"

SLOT="0"
LICENCES="MIT"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen[>=1.6.0]
            app-text/xmlto
            dev-libs/libxslt
            media-gfx/graphviz[>=2.26.0]
        )
    build+run:
        dev-libs/expat
        dev-libs/libffi:=
        dev-libs/libxml2:2.0
        !x11-dri/mesa[<18.0.3] [[
            description = [ wayland imported libwayland-egl from mesa ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Disable-a-test-which-uses-ptrace.patch
)

DEFAULT_SRC_TEST_PARAMS=( -j1 )

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddtd_validation=true
    -Dlibraries=true
    -Dscanner=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc documentation'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

wayland_src_prepare() {
    meson_src_prepare

    # Would be nice if meson had --docdir, upstream issue #825
    edo sed -e "s/meson.project_name()/& + '-${PVR}'/" -i doc/meson.build
}

wayland_src_test() {
    esandbox allow_net unix:/tmp/wayland-tests-*/wayland-*

    # Or else the path is too long for the test buffer
    XDG_RUNTIME_DIR=/tmp meson_src_test

    esandbox disallow_net unix:/tmp/wayland-tests-*/wayland-*
}

